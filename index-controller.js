var app = angular.module('app', ['ngMaterial', 'ngMessages'])
    .config(function ($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('blue-grey')
            .accentPalette('blue-grey');
    }).controller('indexCtrl', ['$scope', function ($scope) {
        $scope.init = function () {
            console.log('oi');
        };

        $scope.init();
    }]);